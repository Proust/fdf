#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: apietush <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/09/11 16:59:50 by apietush          #+#    #+#              #
#    Updated: 2017/09/11 16:59:57 by apietush         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = fdf
SRCS = sources
FLAGS = -Wall -Wextra -Werror -o
SRC = $(SRCS)/main.c \
		$(SRCS)/linkerbr.c \
		$(SRCS)/reader.c \
		$(SRCS)/rotate.c \
		$(SRCS)/draw_points.c \
		$(SRCS)/get_next_line.c \
		$(SRCS)/help_err.c

OBJ = $(SRC:.c=.o)
LIBFT = ./libft
LIB_MLX = ./mlx/libmlx.a

all: $(NAME)

$(NAME): $(OBJ)
		make -C $(LIBFT)
		make -C ./mlx
		gcc $(FLAGS) $(NAME) $(OBJ) $(LIBFT)/libft.a $(LIB_MLX) -framework OpenGL -framework AppKit

%.o: %.c
		gcc -c $(FLAGS) $@ $<

clean:
		rm -f $(OBJ)
		make clean -C $(LIBFT)
		make clean -C ./mlx

fclean: clean
		rm -f $(NAME)
		rm -f $(OBJ)
		make fclean -C $(LIBFT)

re:		fclean all
