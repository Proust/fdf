/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/15 22:02:01 by apietush          #+#    #+#             */
/*   Updated: 2017/08/15 22:02:04 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

void			ft_cleaner(t_gov *no, int i)
{
	if (i == 1)
		ft_strdel(&no->temp);
	ft_strdel(&no->temp2);
	ft_strdel(&no->temp3);
}

int				ft_find_line(char *tmp, t_list *lst, char **line)
{
	t_gov	no;

	no.i = 0;
	no.j = 0;
	no.temp = lst->content;
	if ((tmp[0] == '\0') && !no.temp[0])
		return (0);
	while (no.temp[no.i] != '\n' && no.temp[no.i] != '\0')
		no.i++;
	while (tmp[no.j] != '\n' && tmp[no.j] != '\0')
		no.j++;
	no.temp2 = ft_strsub(lst->content, 0, no.i);
	no.temp3 = ft_strsub(tmp, 0, no.j);
	*line = ft_strjoin(no.temp2, no.temp3);
	ft_cleaner(&no, 0);
	if (tmp[0] == '\0')
		no.i++;
	no.temp2 = ft_strsub(tmp, no.j + 1, ft_strlen(tmp) - no.j);
	no.temp3 = ft_strsub(lst->content, no.i, ft_strlen(lst->content) - no.i);
	lst->content = ft_strjoin(no.temp3, no.temp2);
	ft_cleaner(&no, 1);
	if (**line == '\n')
		*line = ft_strnew(0);
	return (line ? 1 : 0);
}

int				ft_read_line(t_list *lst, char buff[BUFF_SIZE + 1], char **tmp)
{
	int				read_bytes;
	char			*tmp_tmp;

	*tmp = ft_strnew(0);
	if (!lst->content)
		lst->content = ft_strnew(0);
	while ((ft_strchr(*tmp, '\n') == 0) && (!ft_strchr(lst->content, '\n')) &&
		(read_bytes = read(lst->content_size, buff, BUFF_SIZE)) > 0)
	{
		buff[read_bytes] = '\0';
		if (tmp[0] == '\0')
			*tmp = ft_strdup(buff);
		tmp_tmp = *tmp;
		*tmp = ft_strjoin(tmp_tmp, buff);
		ft_strdel(&tmp_tmp);
		if (ft_strchr(buff, '\n'))
			break ;
	}
	return (*tmp ? 1 : 0);
}

static t_list	*look_for_fd(size_t fd, t_list **head)
{
	t_list			*tmp;
	t_list			*new;

	tmp = *head;
	if (!*head)
	{
		*head = ft_lstnew(0, 0);
		new = *head;
	}
	else
	{
		new = *head;
		while (new->next && new->content_size != fd)
			new = new->next;
		if (new->content_size == fd)
			return (new);
		new = ft_lstnew(0, 0);
		new->content_size = fd;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
	new->content_size = fd;
	return (new);
}

int				get_next_line(const int fd, char **line)
{
	char			buff[BUFF_SIZE + 1];
	int				read_bytes;
	char			*tmp;
	static t_list	*head;
	t_list			*new;

	if (!(fd >= 0 && (read_bytes = read(fd, buff, 0)) >= 0 && BUFF_SIZE > 0))
		return (-1);
	ft_bzero(buff, BUFF_SIZE);
	new = look_for_fd((size_t)fd, &head);
	if (ft_read_line(new, buff, &tmp) && ft_find_line(tmp, new, line))
	{
		ft_strdel(&tmp);
		return (1);
	}
	else
		return (0);
}
