/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help_err.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 20:16:56 by apietush          #+#    #+#             */
/*   Updated: 2017/10/04 20:16:57 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

int		exit_x(void *par)
{
	par = NULL;
	exit(1);
	return (0);
}

int		key_hook(int keycode, t_mlx *mlx)
{
	if (keycode == 53)
	{
		mlx_destroy_window(mlx->mlx_ptr, mlx->win_ptr);
		exit(1);
	}
	return (0);
}

t_mlx	**pre_colour(char **line, t_mlx **mlx, int x, int y)
{
	(*line)++;
	if ((*mlx)->flag_colour != 0)
	{
		(*mlx)->map[y][x].colour = (*mlx)->bonus_colour;
		while (**line && **line != ' ')
			(*line)++;
	}
	else
		(*mlx)->map[y][x].colour = write_colour(line);
	return (mlx);
}
