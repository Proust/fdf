/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_points.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/05 21:56:00 by apietush          #+#    #+#             */
/*   Updated: 2017/10/05 21:56:05 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

t_mlx	*change_coord(t_mlx *mlx)
{
	int		a;
	int		b;

	a = 0;
	while (a < mlx->height)
	{
		b = 0;
		while (b < mlx->width)
		{
			mlx->map[a][b].x -= mlx->width / 2;
			mlx->map[a][b].y -= mlx->height / 2;
			if (mlx->extraflag == 1)
				mlx->map[a][b].z = mlx->map[a][b].z / 10;
			else
				mlx->map[a][b].z = mlx->map[a][b].z / 2;
			b++;
		}
		a++;
	}
	return (mlx);
}

t_mlx	*setting_scaling(t_mlx *mlx)
{
	if (mlx->width > mlx->height)
		mlx->scale = ((WIN_WIDTH - WIN_WIDTH / 2) / mlx->width);
	else
		mlx->scale = ((WIN_HEIGHT - WIN_HEIGHT / 2) / mlx->height);
	mlx->cent_w = WIN_WIDTH / 2;
	mlx->cent_h = WIN_HEIGHT / 2;
	return (mlx);
}

void	draw_h(t_mlx *mlx, t_braz **br)
{
	int		a;
	int		b;

	a = 0;
	while (a < mlx->height)
	{
		b = 0;
		while (b < mlx->width)
		{
			mlx_pixel_put(mlx->mlx_ptr, mlx->win_ptr, (mlx->map[a][b].x
				* mlx->scale) + mlx->cent_w, (mlx->map[a][b].y * mlx->scale)
				+ mlx->cent_h, mlx->map[a][b].colour);
			if (b + 1 < mlx->width)
			{
				(*br)->x0 = (mlx->map[a][b].x * mlx->scale) + mlx->cent_w;
				(*br)->y0 = (mlx->map[a][b].y * mlx->scale) + mlx->cent_h;
				(*br)->x1 = (mlx->map[a][b + 1].x * mlx->scale) + mlx->cent_w;
				(*br)->y1 = (mlx->map[a][b + 1].y * mlx->scale) + mlx->cent_h;
				liner(mlx, br, &mlx->map[a][b]);
			}
			b++;
		}
		a++;
	}
}

void	draw_v(t_mlx *mlx, t_braz **br)
{
	int		a;
	int		b;

	a = 0;
	while (a < mlx->height)
	{
		b = 0;
		while (b < mlx->width)
		{
			mlx_pixel_put(mlx->mlx_ptr, mlx->win_ptr, (mlx->map[a][b].x
				* mlx->scale) + mlx->cent_w, (mlx->map[a][b].y * mlx->scale)
				+ mlx->cent_h, mlx->map[a][b].colour);
			if (a + 1 < mlx->height)
			{
				(*br)->x0 = (mlx->map[a][b].x * mlx->scale) + mlx->cent_w;
				(*br)->y0 = (mlx->map[a][b].y * mlx->scale) + mlx->cent_h;
				(*br)->x1 = (mlx->map[a + 1][b].x * mlx->scale) + mlx->cent_w;
				(*br)->y1 = (mlx->map[a + 1][b].y * mlx->scale) + mlx->cent_h;
				liner(mlx, br, &mlx->map[a][b]);
			}
			b++;
		}
		a++;
	}
}

void	draw_points(t_mlx *mlx)
{
	t_braz *br;

	mlx->mlx_ptr = mlx_init();
	mlx->win_ptr = mlx_new_window(mlx->mlx_ptr, WIN_WIDTH, WIN_HEIGHT, "FDF");
	mlx = change_coord(mlx);
	mlx = setting_scaling(mlx);
	mlx = rotate(mlx, -5.84, 'x');
	mlx = rotate(mlx, 5.84, 'y');
	mlx = rotate(mlx, 0.61, 'z');
	draw_h(mlx, &br);
	draw_v(mlx, &br);
	mlx_hook(mlx->win_ptr, 17, 1L << 17, exit_x, mlx);
	mlx_hook(mlx->win_ptr, 2, 5, key_hook, (void *)mlx);
	mlx_loop(mlx->mlx_ptr);
}
