/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 19:55:52 by apietush          #+#    #+#             */
/*   Updated: 2017/10/04 19:55:54 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

void	rotate_by_axis(t_map *point, char axis, double angle)
{
	double x;
	double y;
	double z;

	x = point->x;
	y = point->y;
	z = point->z;
	if (axis == 'x')
	{
		point->y = y * cos(angle) + z * -sin(angle);
		point->z = y * sin(angle) + z * cos(angle);
	}
	else if (axis == 'y')
	{
		point->x = x * cos(angle) + z * sin(angle);
		point->z = x * -sin(angle) + z * cos(angle);
	}
	else
	{
		point->x = x * cos(angle) + y * -sin(angle);
		point->y = x * sin(angle) + y * cos(angle);
	}
}

t_mlx	*rotate(t_mlx *mlx, double angle, char axis)
{
	int		i;
	int		j;

	i = 0;
	while (i < mlx->height)
	{
		j = 0;
		while (j < mlx->width)
		{
			rotate_by_axis(&mlx->map[i][j], axis, angle);
			j++;
		}
		i++;
	}
	return (mlx);
}
