/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   linkerbr.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 19:55:26 by apietush          #+#    #+#             */
/*   Updated: 2017/10/04 19:55:28 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

int		ft_abs(int c)
{
	if (c > 0)
		return (c);
	else
		return (c *= -1);
}

void	y_minor(t_braz **br, t_map *point, t_mlx *mlx)
{
	(*br)->d = ((*br)->dy << 1) - (*br)->dx;
	(*br)->d1 = (*br)->dy << 1;
	(*br)->d2 = ((*br)->dy - (*br)->dx) << 1;
	mlx_pixel_put(mlx->mlx_ptr, mlx->win_ptr,
				(*br)->x0, (*br)->y0, point->colour);
	(*br)->x = (*br)->x0 + (*br)->sx;
	(*br)->y = (*br)->y0;
	(*br)->i = 1;
	while ((*br)->i <= (*br)->dx)
	{
		if ((*br)->d > 0)
		{
			(*br)->d += (*br)->d2;
			(*br)->y += (*br)->sy;
		}
		else
			(*br)->d += (*br)->d1;
		mlx_pixel_put(mlx->mlx_ptr, mlx->win_ptr,
					(*br)->x, (*br)->y, point->colour);
		(*br)->i++;
		(*br)->x += (*br)->sx;
	}
}

void	y_major(t_braz **br, t_map *point, t_mlx *mlx)
{
	(*br)->d = ((*br)->dx << 1) - (*br)->dy;
	(*br)->d1 = (*br)->dx << 1;
	(*br)->d2 = ((*br)->dx - (*br)->dy) << 1;
	mlx_pixel_put(mlx->mlx_ptr, mlx->win_ptr,
				(*br)->x0, (*br)->y0, point->colour);
	(*br)->y = (*br)->y0 + (*br)->sy;
	(*br)->x = (*br)->x0;
	(*br)->i = 1;
	while ((*br)->i <= (*br)->dy)
	{
		if ((*br)->d > 0)
		{
			(*br)->d += (*br)->d2;
			(*br)->x += (*br)->sx;
		}
		else
			(*br)->d += (*br)->d1;
		mlx_pixel_put(mlx->mlx_ptr, mlx->win_ptr, (*br)->x,
					(*br)->y, point->colour);
		(*br)->i++;
		(*br)->y += (*br)->sy;
	}
}

void	liner(t_mlx *mlx, t_braz **br, t_map *point)
{
	(*br)->dx = ft_abs((*br)->x1 - (*br)->x0);
	(*br)->dy = ft_abs((*br)->y1 - (*br)->y0);
	(*br)->sx = (*br)->x1 >= (*br)->x0 ? 1 : -1;
	(*br)->sy = (*br)->y1 >= (*br)->y0 ? 1 : -1;
	if ((*br)->dy <= (*br)->dx)
		y_minor(br, point, mlx);
	else
		y_major(br, point, mlx);
}
