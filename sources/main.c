/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/05 21:03:24 by apietush          #+#    #+#             */
/*   Updated: 2017/10/05 21:03:27 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

int		reader1(t_mlx **mlx, char *argv)
{
	int		fd;
	char	*line;

	(*mlx)->height = 0;
	if ((fd = open(argv, O_RDONLY)) < 0)
	{
		ft_putstr("file error");
		exit(0);
	}
	while (get_next_line(fd, &line) > 0)
		(*mlx)->height++;
	free(line);
	close(fd);
	if ((fd = open(argv, O_RDONLY)) < 0)
	{
		ft_putstr("file error");
		exit(0);
	}
	if (reader2(fd, mlx, line))
		return (1);
	return (1);
}

void	usage_error(void)
{
	ft_putstr("Usage: ./fdf.out map (0x??++++ or try secret pass (cute)\n");
	exit(0);
}

t_mlx	*check_colour_bonus(char *str, t_mlx *mlx)
{
	int		count;

	count = 0;
	if (ft_strcmp(str, "cute") == 0)
	{
		mlx->flag_colour = 777;
		mlx->bonus_colour = 16738740;
	}
	else if (str[0] == '0' && str[1] == 'x')
	{
		str += 2;
		while (*str)
		{
			((*str >= 'A' && *str <= 'F') || (*str >= 'a' && *str <= 'f')
				|| (ft_isdigit(*str))) && count <= 5 ? mlx->flag_colour = 1
				: usage_error();
			str++ && count++;
		}
		if (count <= 1)
			usage_error();
	}
	else
		usage_error();
	return (mlx);
}

int		main(int argc, char **argv)
{
	t_mlx	*mlx;

	mlx = (t_mlx*)malloc(sizeof(t_mlx));
	if (argc != 2 && argc != 3)
		usage_error();
	if (argc == 3)
	{
		mlx = check_colour_bonus(argv[2], mlx);
		if (mlx->flag_colour == 1)
			mlx->bonus_colour = write_colour(&argv[2]);
	}
	reader1(&mlx, argv[1]);
	draw_points(mlx);
	return (0);
}
