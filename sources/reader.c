/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reader.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 19:55:40 by apietush          #+#    #+#             */
/*   Updated: 2017/10/04 19:55:43 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

long	write_colour(char **line)
{
	int		i;

	i = 0;
	*line += 2;
	while (**line && **line != ' ')
	{
		i = i * 16;
		if (**line >= 'A' && **line <= 'F')
			i = i + **line - 'A' + 10;
		else
			i += ft_isdigit(**line) ? **line - '0' : **line - 'a' + 10;
		(*line)++;
	}
	return (i);
}

int		is_space(char str)
{
	return (str == ' ' || str == '\t' || str == '\n' || str == '\0');
}

int		count_words(char *str)
{
	int		i;
	int		count;

	i = 0;
	count = 0;
	while (str[i])
	{
		if (!(is_space(str[i])) && (is_space(str[i + 1])))
			count++;
		i++;
	}
	return (count);
}

int		fill_map(char *line, t_mlx **mlx, int x, int y)
{
	while (*line)
	{
		if (ft_isdigit(*line) || (*line == '-' && ft_isdigit(*(line + 1))))
		{
			(*mlx)->map[y][x].x = x;
			(*mlx)->map[y][x].y = y;
			(*mlx)->map[y][x].z = ft_atoi(line);
			if ((*mlx)->map[y][x].z > 20)
				(*mlx)->extraflag = 1;
			while (*line == '-' || ft_isdigit(*line))
				line++;
			if (*line == ',')
				mlx = pre_colour(&line, mlx, x, y);
			else if ((*mlx)->flag_colour != 0)
				(*mlx)->map[y][x].colour = (*mlx)->bonus_colour;
			else
				(*mlx)->map[y][x].colour = 16777215;
			x++;
		}
		else if (*line == ' ')
			(line)++;
		else
			return (-1);
	}
	return (x);
}

int		reader2(int fd, t_mlx **mlx, char *line)
{
	int		x;
	int		y;
	int		x_d;

	x = 0;
	y = 0;
	(*mlx)->map = (t_map**)malloc(sizeof(t_map*) * (*mlx)->height);
	while (get_next_line(fd, &line) >= 1)
	{
		(*mlx)->width = count_words(line);
		(*mlx)->map[y] = (t_map*)malloc(sizeof(t_map) * (*mlx)->width);
		x = 0;
		x = fill_map(line, mlx, x, y);
		if (y > 0)
		{
			if (x_d != x)
			{
				ft_putstr("wrong map");
				exit(0);
			}
		}
		x_d = x;
		y++;
	}
	return (1);
}
