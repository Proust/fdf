/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/05 22:01:46 by apietush          #+#    #+#             */
/*   Updated: 2017/10/05 22:01:53 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# define WIN_HEIGHT 1000
# define WIN_WIDTH 1500
# define BUFF_SIZE 32

# include "mlx.h"
# include "../libft/libft.h"
# include <fcntl.h>
# include <math.h>
# include <stdio.h>

typedef struct		s_gov
{
	int				i;
	int				j;
	char			*temp;
	char			*temp2;
	char			*temp3;
}					t_gov;

typedef struct		s_map
{
	double			x;
	double			y;
	double			z;
	int				colour;
}					t_map;

typedef struct		s_mlx
{
	t_map			**map;
	void			*mlx_ptr;
	void			*win_ptr;
	int				height;
	int				width;
	int				coof_a;
	int				coof_b;
	int				scale;
	int				cent_h;
	int				cent_w;
	int				extraflag;
	int				flag_colour;
	int				bonus_colour;
}					t_mlx;

typedef struct		s_braz
{
	int				dx;
	int				dy;
	int				sx;
	int				sy;
	int				d;
	int				d1;
	int				d2;
	int				x;
	int				y;
	int				i;
	int				x0;
	int				y0;
	int				x1;
	int				y1;
}					t_braz;

t_mlx				**pre_colour(char **line, t_mlx **mlx, int x, int y);
int					key_hook(int keycode, t_mlx *mlx);
int					get_next_line(const int fd, char **line);
long				write_colour(char **line);
int					exit_x(void *par);
void				liner(t_mlx *mlx, t_braz **br, t_map *point);
t_mlx				*rotate(t_mlx *mlx, double angle, char axis);
int					get_next_line(const int fd, char **line);
int					reader2(int fd, t_mlx **mlx, char *line);
void				draw_points(t_mlx *mlx);

#endif
