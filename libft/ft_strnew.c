/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/04 21:43:33 by apietush          #+#    #+#             */
/*   Updated: 2016/12/12 16:12:22 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew(size_t size)
{
	char	*new;
	size_t	i;

	i = 0;
	if ((new = (char*)malloc(size + 1)) == NULL)
		return (NULL);
	while (size--)
	{
		new[i] = '\0';
		i++;
	}
	new[i] = '\0';
	return (new);
}
