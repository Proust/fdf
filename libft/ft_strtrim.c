/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/07 16:34:38 by apietush          #+#    #+#             */
/*   Updated: 2016/12/07 23:00:22 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	char	*new;
	size_t	i;
	size_t	len;
	size_t	j;

	i = 0;
	if (!s)
		return (NULL);
	len = ft_strlen(s);
	while (s[len - 1] == ' ' || s[len - 1] == '\n' || s[len - 1] == '\t')
		len--;
	while (s[i] && (s[i] == ' ' || s[i] == '\n' || s[i] == '\t') && (len > 0))
	{
		len--;
		i++;
	}
	if ((new = (char*)malloc(sizeof(char) * len + 1)) == NULL)
		return (NULL);
	j = 0;
	while (j < len)
		new[j++] = s[i++];
	new[j] = '\0';
	return (new);
}
